const knex = require('../../../database/connection');

const verifyAuthentication = (req, res, next) => {
    if (req.session.user) {
        return next();
    }

    return res.redirect('/login');
}

const verifyNotAuthtication = (req, res, next) => {
    if (!req.session.user) {
        return next();
    }

    return res.redirect('/');
}

const decentralizationBoss = async (req, res, next) => {
  
    try {
        const user_role = req.session.user.role_id;
        if (user_role != 1) {
            if (user_role == 2) {
                next();
            } else {
                return res.send('ban deo du tuoi');
            }
        } else {
            return res.send('you are only admin');
        }
    } catch (err) {
        console.log('Loi cathch',err);
    }
};


const decentralizationUser = async (req, res, next) => {
  
    try {
        const user_role = req.session.user.role_id;
        if (user_role == 1 || user_role== 2){
            next();
        } else {
            return res.send('you are not permitted to access this page');
        }
    } catch (err) {
        console.log('Loi cathch',err);
    }
};
module.exports = { verifyAuthentication, verifyNotAuthtication, decentralizationBoss, decentralizationUser };
