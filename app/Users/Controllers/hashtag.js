const getSlug = require('speakingurl');
const knex = require('../../../database/connection');

const create = async (req, res) => {
  try {
    const { hashtag } = req.body;
    console.log('asdasd',hashtag);
    const slug = getSlug(`${hashtag}-${Date.now()}`);
    const test = await knex('hashtags').where('name', hashtag).first();
    if (test) {
      return res.redirect('/hashtag/create');
    }
    await knex('hashtags').insert({
      slug,
      name: hashtag,
    });
    return res.redirect('/hashtag/manage');
  } catch (err) {
    console.log('Loi---', err);
  }
};

const manage = async (req, res) => {
  try {
    const query = knex('hashtags').select('*');
    const { q } = req.params;
    if (q) {
      await query.where('name', 'like', 'q');
    }
    const tags = await query;
    return res.render('app/admin/Hashtag/manage', { namePage: 'Hashtag Manage', tags });
  } catch (err) {
    console.log('Loi manage -----', err);
  }
};

const destroy = async (req, res) => {
  try {
    const { slug } = req.params;
    console.log('slug', slug),
    await knex('hashtags').where('slug', slug).del();
    return res.json({ type: 'success' });
  } catch (err) {
    console.log('Loi destroy', err);
  }
};
const update = async (req, res) => {
    try {
      const data = req.body;
      console.log(data);
      const { slug } = req.params;
      const Newslug = getSlug(`${data.tag}-${Date.now()}`);
      await knex('hashtags').where('slug', slug).update({
        name: data.tag,
        slug: Newslug,
      });
      return res.json({ type: 'success' });
    } catch (err) {
      console.log('Loi update', err);
    }
  };
module.exports = { create, manage, destroy, update };
