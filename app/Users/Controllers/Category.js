const getSlug = require('speakingurl');
const knex = require('../../../database/connection');

const Create = async (req, res) => {
  try {
    const { category } = req.body;
    const slug = getSlug(`${category}-${Date.now()}`);
    const test = await knex('categories').where('name', category).first();
    if (test) {
      return res.redirect('/createCategory');
    }

    await knex('categories').insert({
      slug,
      name: category,
    });
    return res.redirect('/category/manage');
  } catch (err) {
    console.log('Loi---', err);
  }
};

const manage = async (req, res) => {
  try {
    const query = knex('categories').select('*');
    const { q } = req.params;
    if (q) {
      await query.where('name', 'like', 'q');
    }
    const categories = await query;
    return res.render('app/admin/Category/manage', { namePage: 'Categories Manage', categories });
  } catch (err) {
    console.log('Loi manage -----', err);
  }
};

const destroy = async (req, res) => {
  try {
    const { slug } = req.params;
    console.log('slug', slug),
    await knex('categories').where('slug', slug).del();
    return res.json({ type: 'success' });
  } catch (err) {
    console.log('Loi destroy', err);
  }
};

module.exports = { Create, manage, destroy };
