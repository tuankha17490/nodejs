const getSlug = require('speakingurl');
const knex = require('../../../database/connection');
const { resizeImg } = require('../../../utils/imageResize');
const { s3Upload } = require('../../../services/s3Upload');

const postImg = async (req, res) => {
  const images = req.files;
  for (image of images) {
    const resizeImage = await resizeImg(image);
    const url = await s3Upload(resizeImage, image);
    return res.json({ location: url });
  }
};

const create = async (req, res) => {
  try {
    const data = req.body;
    console.log('asdasd', data);
    console.log('test', data['hashtag[]'][0]);
    const slug = getSlug(`${data.category}-${data.title}-${Date.now()}`);
    const cateId = await knex('categories').where('name', data.category).select('id');
    const authorId = await knex('users').where('email', req.session.user.email).select('id');
    await knex('posts').insert({
      title: data.title,
      slug,
      content: data.content,
      category_id: cateId[0].id,
      Author_id: authorId[0].id,
    });
    const postId = await knex('posts').where('slug',slug).select('*');
    console.log('postid',postId);
    for (let i = 0; i < data['hashtag[]'].length; i++) {
      const tagId = await knex('hashtags').where('name',data['hashtag[]'][i]).select('id');
      await knex('hashtag_post').insert({
        post_id: postId[0].ID,
        hashtag_id: tagId[0].id,
      })
    }
    return res.json({ type: 'success' });
  } catch (err) {
    console.log('Loi catch', err);
  }
};

const manage = async (req, res) => {
  const query = knex('posts').select('*');
  const { q } = req.params;
  if (q) {
    await query.where('title', q);
  }
  const posts = await query;
  return res.render('app/admin/Posts/manage', { namePage: 'Posts Table', posts });

};

const destroy = async (req, res) => {
  try {
    const { slug } = req.params;
    await knex('posts').where('slug', slug).del();
    return res.json({ type: 'success' });
  } catch (err) {
    console.log('loi', err);
    return res.json({ type: 'error' });

  }
};

const update = async (req, res) => {
  try {
    const data = req.body;
    const { slug } = req.params;
    const cateId = await knex('posts').where('slug', slug).select('category_id');
    console.log('iddddd', cateId);
    const category = await knex('categories').where('id', cateId[0].category_id).select('name');
    console.log('asdasdasd', category);
    const Newslug = getSlug(`${category[0].name}-${data.title}-${Date.now()}`);
    await knex('posts').where('slug', slug).update({
      title: data.title,
      slug: Newslug,
    });
    return res.json({ type: 'success' });
  } catch (err) {
    console.log('Loi update', err);
  }
};

module.exports = {
 postImg, create, manage, destroy, update 
};
