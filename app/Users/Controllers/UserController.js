const knex = require('../../../database/connection');

const list = async (req, res) => {
  const { q } = req.query;
  const query = knex('users').select('id', 'fullname', 'email', 'created_at');
  if (q) {
    query.where('email', 'like', `%${q}%`);
  }
  const users = await query;
  return res.render('app/admin/user/manage', { namePage: 'userManage', users });
};

const update = async (req, res) => {
  try {
    const data = req.body;
    console.log('data====', data);
    const { id } = req.params;
    console.log('id=======', id);
    await knex('users').where('id', id).update({
      fullname: data.fullname,
      email: data.email,
    });
    return res.json({ type: 'success' });
  } catch (err) {
    console.log('Loi catch: ', err);
  }
};

const destroy = async (req, res) => {
  try {
    const { id } = req.params;
    console.log('id------', id);
    await knex('users').where('id', id).del();
    return res.json({ type: 'success' });
  } catch (err) {
    console.log('loi:', err);
    return res.json({ type: 'error' });
  }
};

const upgrade = async (req, res) => {
  try {
    const { id } = req.params;
    const checkId = await knex('users').where('id', id).first('role_id');
    const roleid = await knex('roles').where('name', 'like', 'admin').first('id');
    if (checkId == 2) {
      return res.redirect('/user');
    }
    await knex('users').where('id', id).update({ role_id: roleid.id });
    return res.json({ type: 'success' });
  } catch (err) {
    console.log('Loi catch!', err);
  }
};
module.exports = {
  list, update, destroy, upgrade,
};
