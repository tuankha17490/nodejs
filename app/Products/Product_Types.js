const knex = require('../../database/connection');
const getSlug = require('speakingurl');

const productCreate = async (req, res) => {
    const { productType } = req.body;
    const user = await knex('product_types').where('name',productType).first()
    let slug = getSlug(productType + '-' + Date.now())
    if (user) {
        return res.redirect('/productsType/create');
    }
    await knex('product_types').insert({ name: productType, slug });
    return res.redirect('/productsType/manage');
};

const list = async (req, res) => {
    let {q} = req.query; 
    let query =  knex('product_types').select('id','name','slug','created_at');
    if (q) {
        query.where('name', 'like', `%${q}%`)
    }
    const products = await query;
    return res.render('app/admin/products/product-type-table', { namePage:'Product Type Manage', products }) ;
};
const destroy = async (req, res) => {
   try {
    const { slug } = req.params;
    await knex('product_types').where('slug',slug).del();
    return res.json({ type:'success' });
   } catch (err) {
       console.log('loi',err);
       return res.json({type:'error'});
       
   }
}

module.exports = {productCreate,list,destroy}