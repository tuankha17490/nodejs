const getSlug = require('speakingurl');
const knex = require('../../database/connection');
const { s3Upload } = require('../../services/s3Upload');
const { resizeImg } = require('../../utils/imageResize');

const create = async (req, res) => {
  try {
    const data = req.body;
    const images = req.files;
    const user = await knex('products').where('NAME', data.nameProduct).first();
    const typeid = await knex('product_types').where('name', data.category).select('id');
    const authorid = await knex('users').where('email', req.session.user.email).select('id');
    const slug = getSlug(`${data.nameProduct}-${Date.now()}`);
    if (user) {
      return res.redirect('/products/create');
    }
    await knex('products').insert({
      NAME: data.nameProduct,
      SLUG: slug,
      Price: data.price,
      Description: data.description,
      Product_type_id: typeid[0].id,
      Author_id: authorid[0].id,
    });
    const productID = await knex('products').where('name', data.nameProduct).select('id');

    for (const image of images) {
      const imageResize = await resizeImg(image);
      const url = await s3Upload(imageResize, image);
      await knex('images').insert({
        url,
        Product_id: productID[0].id,
      });
    }
    return res.redirect('/products/manage');
  } catch (err) {
    console.log('LOI', err);
  }
};

const list = async (req, res) => {
  const query = knex('products').select('slug', 'name', 'price', 'description', 'author_id', 'created_at');
  const { q } = req.query;
  if (q) {
    await query.where('name', 'like', `%${q}%`);
  }

  const products = await query;
  return res.render('app/admin/products/products-table', { namePage: 'Table Of Products', products });
};

const destroy = async (req, res) => {
  try {
    const { slug } = req.params;
    await knex('products').where('slug', slug).del();
    return res.json({ type: 'success' });
  } catch (err) {
    console.log('loi', err);
    return res.json({ type: 'error' });

  }
};

const update = async (req, res) => {
  try {
    const data = req.body;
    console.log('data', data);
    const { slug }  = req.params;
    console.log('slug', slug);
    const Newslug = getSlug(`${data.name}-${Date.now()}`);
    console.log('new slug', Newslug);
    await knex('products').where('slug', slug).update({
      slug: Newslug,
      name: data.name,
      price: data.price,
      description: data.description,
    });
    return res.json({ type: 'success' });
  } catch (err) {
    console.log('Loi update', err);
  }
};

module.exports = {
  create, list, destroy, update,
};
