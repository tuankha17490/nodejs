const bcrypt= require('bcrypt');
const salt = bcrypt.genSaltSync(10);
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      // Inserts seed entries
      return knex('users').insert([
        {
          id: 1,
          fullname: 'tuan kha',
          password:bcrypt.hashSync('17041999', salt),
          email:'tuankha1749@gmail.com',
          role_id:2
        }
        
      ]);
    });
};
