exports.up = function(knex) {
   return knex.schema
      .createTable('users', function (table) {
         table.increments('id').primary();
         table.string('fullname', 255).notNullable();
         table.string('password', 255).notNullable();
         table.string('email', 255).notNullable();
         table.integer('role_id').unsigned();
         table.foreign('role_id').references('id').on('roles') .onDelete('CASCADE');
         table.timestamp('created_at').defaultTo(knex.fn.now());
         table.timestamp('updated_at').defaultTo(knex.fn.now());
      });
};

exports.down = function(knex) {
   return knex.schema
      .dropTable("users");
};
