
exports.up = function(knex) {
    return knex.schema
    .createTable('hashtags', function(table) {
        table.increments('ID').primary();
        table.string('slug',255).notNullable().unique();
        table.string('name',255).notNullable();
        table.timestamp('Created_at').defaultTo(knex.fn.now());
    })
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('hashtags');
};