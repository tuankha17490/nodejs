exports.up = function(knex) {
    return knex.schema
    .createTable('hashtag_post', function(table) {
        table.increments('ID').primary();
        table.integer('post_id').unsigned();
        table.foreign('post_id').references('id').on('posts').onDelete('CASCADE');
        table.integer('hashtag_id').unsigned();
        table.foreign('hashtag_id').references('id').on('hashtags').onDelete('CASCADE');
        table.timestamp('Created_at').defaultTo(knex.fn.now());
        table.timestamp('Updated_at').defaultTo(knex.fn.now());     
    })
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('hashtag_post');
};
