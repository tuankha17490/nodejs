
exports.up = function(knex) {
  return knex.schema
    .createTable('IMAGES', function(table) {
        table.increments('ID').primary();
        table.string('URL',255).notNullable();
        table.integer('Product_id').unsigned();
        table.foreign('Product_id').references('ID').inTable('Products').onDelete('CASCADE')
    })
};

exports.down = function(knex) {
  return knex.schema
    .dropTable('IMAGES');
};
