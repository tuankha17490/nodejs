
exports.up = function(knex) {
    return knex.schema
    .createTable('posts', function(table) {
        table.increments('ID').primary();
        table.string('slug',255).notNullable().unique();
        table.string('title',255).notNullable();
        table.integer('category_id').unsigned();
        table.foreign('category_id').references('id').on('categories').onDelete('CASCADE');
        table.text('content','longtext').notNullable();
        table.integer('Author_id').unsigned();
        table.foreign('Author_id').references('id').on('users').onDelete('CASCADE');
        table.timestamp('Created_at').defaultTo(knex.fn.now());
        table.timestamp('Updated_at').defaultTo(knex.fn.now()); 
    })
};

exports.down = function(knex) {
  return knex.schema
    .dropTable('posts');
};
