
exports.up = function(knex) {
    return knex.schema
    .createTable('roles', function(table) {
        table.increments('id').primary();
        table.string('name',255).notNullable();
        table.timestamp('Created_at').defaultTo(knex.fn.now());
        table.timestamp('Updated_at').defaultTo(knex.fn.now());
    })
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('roles');
};
