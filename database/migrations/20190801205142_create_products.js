
exports.up = function(knex) {
  return knex.schema
    .createTable('Products', function(table) {
        table.increments('ID').primary();
        table.string('SLUG',255).notNullable().unique();
        table.string('Name',255).notNullable();
        table.integer('Product_type_id').unsigned();
        table.foreign('Product_type_id').references('id').on('Product_types').onDelete('CASCADE');
        table.integer('Price').notNullable();
        table.text('Description').notNullable();
        table.integer('Author_id').unsigned();
        table.foreign('Author_id').references('id').on('users').onDelete('CASCADE');
        table.timestamp('Created_at').defaultTo(knex.fn.now());
        table.timestamp('Updated_at').defaultTo(knex.fn.now()); 
    })
};

exports.down = function(knex) {
    return knex.schema
    .dropTable('Products');
};
