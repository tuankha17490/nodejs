const sharp = require('sharp');
const fs = require('fs');

const resizeImg = ( picture ) => {
    const test = fs.readFileSync(picture.path);
    return sharp(test).resize(360, 360).toFile(`${picture.path}`);
}
module.exports = { resizeImg };