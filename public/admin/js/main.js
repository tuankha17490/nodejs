tinymce.init({
  selector: '#content',
  height: 400,
  plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table  paste',
    'wordcount imagetools',
  ],
  toolbar: 'insertfile undo redo | styleselect | fontsizeselect | bold italic strikethrough forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',

  images_upload_handler(blobInfo, success, failure) {
    let xhr; let
      formData;

    xhr = new XMLHttpRequest();
    xhr.withCredentials = false;
    xhr.open('POST', '/postImg');

    xhr.onload = function () {
      let json;

      if (xhr.status != 200) {
        failure(`HTTP Error: ${xhr.status}`);
        return;
      }

      json = JSON.parse(xhr.responseText);

      if (!json || typeof json.location !== 'string') {
        failure(`Invalid JSON: ${xhr.responseText}`);
        return;
      }

      success(json.location);
    };

    formData = new FormData();
    formData.append('file', blobInfo.blob(), blobInfo.filename());

    xhr.send(formData);
  },
});

function init_edit() {
  $('.user-id').click(function () {
    const id = $(this).closest('tr').data('id');
    console.log(id);
    $('.user-edit').click(() => {

      $.ajax({
        url: `/edit/${id}`,
        type: 'PUT',
        dataType: 'JSON',
        data: {
          _method: 'PUT',
          fullname: $('#fullname').val(),
          email: $('#email').val(),
        },
      }).then((res) => {
        if (res.type == 'success') {
          alert('Success');
          window.location.href = '/user';
        }
      });
    });
  });
}

function init_delete() {
  $('.user-delete').click(function () {
    const id = $(this).closest('tr').data('id');
    $.ajax({
      url: `/delete/${id}`,
      type: 'DELETE',
      dataType: 'json',
      data: {
        _method: 'DELETE',
      },
    }).then((res) => {
      if (res.type == 'success') {
        Swal.fire(
          'Good job!',
          'You clicked the button!',
          'success',
        );

      }
    });
  });
}

function productsType_delete() {
  $('.product-type-delete').click(function () {
    const slug = $(this).closest('tr').data('id');
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: `/productsType/delete/${slug}`,
          type: 'DELETE',
          dataType: 'json',
          data: {
            _method: 'DELETE',
          },

        }).then((res) => {
          if (res.type == 'success') {
            Swal.fire(
              {
                title: 'Deleted!',
                text: 'Your file has been deleted.',
                type: 'success',
              },
            ).then(() => {
              location.reload();
            });
          }
        });
      }
    });
  });
}

function products_delete() {
  $('.product-delete').click(function () {
    const slug = $(this).closest('tr').data('id');
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: `/products/delete/${slug}`,
          type: 'DELETE',
          dataType: 'json',
          data: {
            _method: 'DELETE',
          },

        }).then((res) => {
          if (res.type == 'success') {
            Swal.fire(
              {
                title: 'Deleted!',
                text: 'Your file has been deleted.',
                type: 'success',
              },
            ).then(() => {
              location.reload();
            });
          }
        });
      }
    });
  });
}

function products_update() {
  $('.product-confirm').click(function () {
    const slug = $(this).closest('tr').data('id');
    console.log(slug);
    $('.product-edit').click(() => {
      $.ajax({
        url: `/products/update/${slug}`,
        type: 'PUT',
        dataType: 'JSON',
        data: {
          _method: 'PUT',
          name: $('#product-name').val(),
          price: $('#product-price').val(),
          description: $('#product-description').val(),
        },
      }).then((res) => {
        if (res.type == 'success') {
          Swal.fire({
            title: 'Updated',
            text: 'The new information has been updated',
            type: 'success',
          }).then(() => {
            return location.reload();
          });

        }
      });
    });
  });
}

function create_post() {
  $('.postCreate').click(() => {
    console.log('ádasdasdasd',$('#hashtag').val());
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, create it!',
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: '/createPost',
          type: 'POST',
          dataType: 'json',
          data: {
            title: $('#title').val(),
            category: $('#Category').val(),
            content: $('#content').val(),
            hashtag: $('#hashtag').val(),
          },
        }).then((res) => {
          if (res.type == 'success') {
            Swal.fire(
              {
                title: 'Created!',
                text: 'The new post has been created.',
                type: 'success',
              },
            ).then(() => {
              window.location.href = '/posts/manage';
            });
          }
        });
      }
    });
  });
}

function delete_category() {
  $('.category-delete').click(function () {
    const slug = $(this).closest('tr').data('id');
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: `/category/delete/${slug}`,
          type: 'DELETE',
          dataType: 'json',
          data: {
            _method: 'DELETE',
          },

        }).then((res) => {
          if (res.type == 'success') {
            Swal.fire(
              {
                title: 'Deleted!',
                text: 'Your file has been deleted.',
                type: 'success',
              },
            ).then(() => {
              location.reload();
            });
          }
        });
      }
    });
  });
}

function posts_delete() {
  $('.post-delete').click(function () {
    const slug = $(this).closest('tr').data('id');
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: `/posts/delete/${slug}`,
          type: 'DELETE',
          dataType: 'json',
          data: {
            _method: 'DELETE',
          },

        }).then((res) => {
          if (res.type == 'success') {
            Swal.fire(
              {
                title: 'Deleted!',
                text: 'Your file has been deleted.',
                type: 'success',
              },
            ).then(() => {
              location.reload();
            });
          }
        });
      }
    });
  });
}

function post_update() {
  $('.post-confirm').click(function () {
    const slug = $(this).closest('tr').data('id');
    console.log(slug);
    $('.post-edit').click(() => {
      $.ajax({
        url: `/posts/update/${slug}`,
        type: 'PUT',
        dataType: 'JSON',
        data: {
          _method: 'PUT',
          title: $('#title').val()
        },
      }).then((res) => {
        if (res.type == 'success') {
          Swal.fire({
            title: 'Updated',
            text: 'The new information has been updated',
            type: 'success',
          }).then(() => {
            return location.reload();
          });

        }
      });
    });
  });
}

function hashtag_delete() {
  $('.hashtag-delete').click(function () {
    const slug = $(this).closest('tr').data('id');
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: `/hashtag/delete/${slug}`,
          type: 'DELETE',
          dataType: 'json',
          data: {
            _method: 'DELETE',
          },

        }).then((res) => {
          if (res.type == 'success') {
            Swal.fire(
              {
                title: 'Deleted!',
                text: 'Your file has been deleted.',
                type: 'success',
              },
            ).then(() => {
              location.reload();
            });
          }
        });
      }
    });
  });
}
 
function hashtag_update() {
  $('.tag-confirm').click(function () {
    const slug = $(this).closest('tr').data('id');
    console.log(slug);
    $('.tag-edit').click(() => {
      $.ajax({
        url: `/hashtag/update/${slug}`,
        type: 'PUT',
        dataType: 'JSON',
        data: {
          _method: 'PUT',
          tag: $('#tag').val()
        },
      }).then((res) => {
        if (res.type == 'success') {
          Swal.fire({
            title: 'Updated',
            text: 'The new information has been updated',
            type: 'success',
          }).then(() => {
            return location.reload();
          });

        }
      });
    });
  });
}

function user_upgrade() {
  $('.user-admin').click(function () {
    const id = $(this).closest('tr').data('id');
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, upgrade!',
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url: `/user/upgrade/${id}`,
          type: 'PUT',
          dataType: 'json',
          data: {
            _method: 'PUT',
          },

        }).then((res) => {
          if (res.type == 'success') {
            Swal.fire(
              {
                title: 'Upgraded!',
                text: 'This account is upgraded to admin.',
                type: 'success',
              },
            ).then(() => {
              location.reload();
            });
          }
        });
      }
    });
  });
}

$(document).ready(() => {
  init_edit();
  init_delete();
  user_upgrade()
  productsType_delete();
  create_post();
  delete_category();
  products_delete();
  products_update();
  posts_delete();
  hashtag_delete();
  hashtag_update();
  post_update();
  const searchParams = new URLSearchParams(window.location.search);
  const q = searchParams.get('q');
  $('input[name="q"]').val(q || '');
});
