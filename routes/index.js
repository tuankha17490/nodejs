const express = require('express');
const knex = require('../database/connection');
const bcrypt= require('bcrypt');
const authController = require('../app/Auth/Controllers/AuthController');
const authMiddleware = require('../app/Auth/Middleware/Authentication');
const {validationResult,check}= require('express-validator');
const UserController = require('../app/Users/Controllers/UserController');
const router = express.Router();
const post = require('../app/Users/Controllers/Post');
const multer = require ('multer');
const path = require('path');
const category = require('../app/Users/Controllers/Category');
const hashtag = require('../app/Users/Controllers/hashtag');

/* GET home page. */
router.get('/', authMiddleware.verifyAuthentication, function(req, res, next) {
  res.render('app/admin/general', { namePage: 'General Form Elements' });
});

router.get('/simple', function(req, res, next) {
  res.render('app/admin/simple', { namePage: 'Simple Tables' });
});

router.get('/login', authMiddleware.verifyNotAuthtication, function(req, res, next) {
  res.render('app/clients/login', { namePage: 'Login', titleForm: 'Sign in to start your session', messages: req.flash('messages') });
});

router.post('/login', async (req, res, next) => {
  check('email').trim().isEmail().withMessage('Invalid Email');
  check('password').trim().isLength({min: 5}).withMessage('Invalid Password');
  let errors = validationResult(req);
  if (!errors.isEmpty()) {
    req.flash('messages', errors.array());
    return res.redirect('/login');
  } 
  else {
    const { email, password } = req.body;
    const user = await knex('users').where('email',email).select('*').first();
    if(!user || !bcrypt.compareSync(password, user.password)){
      return res.redirect('/login');
    }
    req.session.user = user;
    return res.redirect('/');
  }
  

});

router.get('/register', function(req, res, next) {
  res.render('app/clients/register', { namePage: 'Register', titleForm: 'Register a new membership' });
});

router.post('/register', async (req, res, next) => {
    const {email} = req.body;
    const user = await knex('users').where('email',email).first();
    if (user){
      return res.redirect('/register');
    }
    const data = req.body;
    delete data.confirmPassword;
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(data.password, salt);
    
    data.password = hash;

    await knex('users').insert(data);
    return res.redirect('/');

});
router.get('/logout', function(req, res, next){
  delete req.session.user;
  return res.redirect('/login');
});

// ------ USERS -------

router.get('/user',authMiddleware.decentralizationUser,UserController.list);

router.put('/edit/:id',authMiddleware.decentralizationBoss,UserController.update);

router.delete('/delete/:id',authMiddleware.decentralizationBoss,UserController.destroy);

router.put('/user/upgrade/:id',authMiddleware.decentralizationBoss,UserController.upgrade);

// ------- POST ------


const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/img/uploads');
  },
  filename: function (req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`);
  },
});

const upload = multer({ storage: storage });
router.get('/post',async(req, res,) => {
  const categories = await knex('categories').select('*');
  const tags = await knex('hashtags').select('*');
  return res.render('app/admin/user/post',{namePage:'Posts', categories, tags });
});
router.post('/postImg',authMiddleware.decentralizationUser,upload.array('file',12),post.postImg);

router.post('/createPost',authMiddleware.decentralizationUser,post.create);
router.get('/posts/manage',authMiddleware.decentralizationUser,post.manage);
router.delete('/posts/delete/:slug',authMiddleware.decentralizationUser,post.destroy);
router.put('/posts/update/:slug',authMiddleware.decentralizationUser,post.update);
// -----------------------------
router.get('/shop/home',async(req, res) => {
  const products = await knex('products').select('*');
  for (let i=0;i<products.length;i++) {
    products[i].images = await knex('images').where('Product_id',products[i].ID).select('URL');
  }
  return res.render('app/clients/index-shop', {namePage: 'Home Shop', products});
})

router.get('/shop/:slug', async(req, res) => {
  const { slug } = req.params;
  const products = await knex('products').where('slug',slug).select('*');
  const category = await knex('product_types').where('id',products[0].Product_type_id).first('name'); 
  const productRelates = await knex('products').where('Product_type_id',products[0].Product_type_id).whereNot('slug',slug).select('*');
  for (let i=0;i<products.length;i++) {
    products.images = await knex('images').where('Product_id',products[i].ID).select('URL');
  }
  for (let i=0;i<productRelates.length;i++) {
    productRelates[i].images = await knex('images').where('Product_id',productRelates[i].ID).select('URL');
  }


  return res.render('app/clients/product-detail',{namePage:'Products Detail',products, category, productRelates});
});

// ------- CATEGORY ------


router.get('/createCategory',authMiddleware.decentralizationUser,function(req, res){
  return res.render('app/admin/Category/create',{ namePage:'Create Category'});
});
router.post('/createCategory',authMiddleware.decentralizationUser,category.Create);
router.get('/category/manage',authMiddleware.decentralizationUser,category.manage);
router.delete('/category/delete/:slug',authMiddleware.decentralizationUser,category.destroy);

// ----- HASHTAG ------
router.get('/hashtag/create',authMiddleware.decentralizationUser,function(req, res){
  return res.render('app/admin/Hashtag/create',{namePage:'Create New Hashtag'});
})
router.post('/hashtag/create',authMiddleware.decentralizationUser,hashtag.create);

router.get('/hashtag/manage',authMiddleware.decentralizationUser,hashtag.manage);

router.delete('/hashtag/delete/:slug',authMiddleware.decentralizationUser,hashtag.destroy);
router.put('/hashtag/update/:slug',authMiddleware.decentralizationUser,hashtag.update);

module.exports = router;
