const express = require('express');
const router = express.Router();
const products = require('../app/Products/Products')
const knex = require('../database/connection');
const multer = require('multer');
const path = require('path');
const authMiddleware = require('../app/Auth/Middleware/Authentication');


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'public/admin/img/uploads');
    },
    filename: function (req, file, cb) {
      cb(null, `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`);
    },
  });
  
const upload = multer({ storage: storage });
  
  
router.get('/create',authMiddleware.decentralizationUser,async(req, res) =>{
    const productsTypes = await knex('product_types').select('*');
    return res.render('app/admin/products/form-products',{namePage:'Create New Product',productsTypes});
});

router.post('/create',authMiddleware.decentralizationUser,upload.array('image', 12),products.create);
router.get('/manage',authMiddleware.decentralizationUser,products.list);
router.put('/update/:slug',authMiddleware.decentralizationUser,products.update);

router.delete('/delete/:slug',authMiddleware.decentralizationUser,products.destroy);



module.exports = router;