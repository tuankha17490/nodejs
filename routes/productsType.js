const express = require('express');
const router = express.Router();
const Product_Types = require('../app/Products/Product_Types');
const authMiddleware = require('../app/Auth/Middleware/Authentication');


/* GET users listing. */
router.get('/create',authMiddleware.decentralizationUser,function(req, res) {
    res.render('app/admin/products/new-product', {namePage:'Create New Product'});
});
router.post('/create',authMiddleware.decentralizationUser,Product_Types.productCreate);
router.get('/manage',authMiddleware.decentralizationUser,Product_Types.list);

router.delete('/delete/:slug',authMiddleware.decentralizationUser,Product_Types.destroy);


module.exports = router;    






