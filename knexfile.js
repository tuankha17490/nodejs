module.exports = {
  development: {
    client: 'mysql',
    connection: { user: 'root', database: 'nodejs' },
    migrations: { directory: "database/migrations", tableName: "migrations" },
    seeds: { directory: "database/seeds" }
  },
  production: { client: 'mysql', connection: process.env.DATABASE_URL }
};