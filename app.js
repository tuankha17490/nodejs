const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const engine = require('ejs-locals');
const session = require('express-session');
const MySQLStore = require('express-mysql-session')(session);
const flash = require('connect-flash');
const methodOverride = require('method-override');
const localHelpers = require('./utils/localHelpers');
const indexRouter = require('./routes/index');
const productsTypeRouter = require('./routes/productsType');
const productsRouter = require('./routes/products');

const app = express();

// view engine setup
app.engine('ejs', engine);
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(flash());
const options = {
  host: 'localhost',
  port: 3306,
  user: 'root',
  password: '',
  database: 'nodejs',
};
const sessionStore = new MySQLStore(options);
app.use(session({
  key: 'nodejs',
  secret: 'nodejs',
  store: sessionStore,
  resave: false,
  saveUninitialized: true,
  connectionLimit: 12,
}));

app.use((req, res, next) => {
  localHelpers(res);
  return next();
});

app.use((req, res, next) => {
  res.locals.handlerError = (messages, key) => {
    if (messages) {
      let msg = '';
      messages.forEach((error) => {
        if (error.param === key) {
          msg = error.msg;
        }
      });
      return msg;
    }
  };
  return next();
});

app.use('/', indexRouter);
app.use('/productsType/', productsTypeRouter);
app.use('/products/', productsRouter);


app.use(methodOverride((req, res) => {
  if (req.body && typeof req.body === 'object' && '_method' in req.body) {
    // look in urlencoded POST bodies and delete it
    var method = req.body._method
    delete req.body._method
    return method
  }
}));
// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error-handler/error');
});

const debug = require('debug')('http'),
   http = require('http'),
   name = 'My App';

// fake app

debug('booting %o', name);

http.createServer((req, res) => {
  debug(req.method + ' ' + req.url);
  res.end('hello\n');
}).listen(3001, () => {
  debug('listening');
});


module.exports = app;
