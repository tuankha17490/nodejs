const AWS = require('aws-sdk');
const fs = require('fs');

const accessKeyId = process.env.AWS_ACCESS_KEY || 'AKIAZ5QW6SBBCXX6XIKG';
const secretAccessKey = process.env.AWS_SECRET_KEY || '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';

AWS.config.update({
  accessKeyId,
  secretAccessKey,
  region: 'ap-southeast-1',
});

const s3 = new AWS.S3();

const s3Upload = async (file,infor) => {
  const params = {
    Bucket: 'sgroupit-test',
    Key: `khale/${file.width}x${file.height}/${infor.filename.toLowerCase()}`,
    Body: fs.readFileSync(`${infor.path}`),
    ContentType: infor.mimetype,
    ACL: 'public-read',
  };
  const data = await s3.upload(params).promise();

  fs.unlinkSync(`${infor.path}`);
  return data.Location;
};

module.exports = { s3Upload };
